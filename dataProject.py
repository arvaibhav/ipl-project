import utils
import ipl_info
import ipl_sql
import psycopg2 as pg2



def get_ipl_data_from_csv():
    # read matches and deliveries csv and store in dictionary
    matches    = utils.get_csv_data('./data/matches.csv')
    deliveries = utils.get_csv_data('./data/deliveries.csv')

    # Plot the number of matches played per year of all the years in IPL.
    # matches_count=ipl_info.count_matches_per_year(matches)
    # utils.plot_bar_chart(matches_count.keys(),matches_count.values(),'matches played per year'.title())

    # Plot a stacked bar chart of matches won of all teams over all the years of IPL.
    # matches_won=ipl_info.total_matches_won_by_team_each_year(matches)
    # print(matches_won['Sunrisers Hyderabad'])
    # print("\n")
    # utils.plot_stacked_bar(matches_won,'Total Matches Won ')

    # For the year 2016 plot the extra runs conceded per team.
    extra_run_conceded=ipl_info.get_extra_run_conceded_by_each_team('2016',matches,deliveries)
    print(extra_run_conceded)
    # utils.plot_bar_chart(extra_run_conceded.keys(),extra_run_conceded.values(),'extra runs conceded per team.'.title())

    # For the year 2015 plot the top economical bowlers.
    # economical_bowlers=ipl_info.get_economical_bowlers_in_year('2015',matches,deliveries)
    # print(economical_bowlers)
    # utils.plot_bar_chart(economical_bowlers.keys(),economical_bowlers.values(),'economical bowlers 2015')

def get_ipl_data_from_sql():

    ipl_data=ipl_sql.IplSql()
    # Plot the number of matches played per year of all the years in IPL.
    # year,count = ipl_data.count_matches_per_year()
    # utils.plot_bar_chart(year,count,'matches played per year'.title())

    # For the year 2016 plot the extra runs conceded per team.
    # team,score = ipl_data.get_extra_run_conceded_by_each_team('2016')
    # print(team,score)
    # utils.plot_bar_chart(team,score,'matches played per year'.title())

    # For the year 2015 plot the top economical bowlers
    # bowlers,bowler_economy_rate  = ipl_data.get_economical_bowlers_in_year('2015')
    # utils.plot_bar_chart(bowlers, bowler_economy_rate, 'economical bowlers 2015')

    # Plot a stacked bar chart of matches won of all teams over all the years of IPL.
    # matches_won=ipl_data.total_matches_won_by_team_each_year()
    # utils.plot_stacked_bar(matches_won, 'Total Matches Won ')

    # Percentage of win in field first decision is greater than bat
    teams,won_percentage=ipl_data.get_macthes_won_percentage_by_each_team_in_toss_decision('field')
    utils.plot_bar_chart(teams,won_percentage, 'Matches Won percentage in field decision')
    teams, won_percentage = ipl_data.get_macthes_won_percentage_by_each_team_in_toss_decision('bat')
    utils.plot_bar_chart(teams, won_percentage, 'Matches Won percentage in batting decision')

get_ipl_data_from_csv()
# get_ipl_data_from_sql()


