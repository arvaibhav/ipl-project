import ipl_info
import unittest
import utils


class TestStringMethods(unittest.TestCase):

    def test_matches_count(self):
        matches = utils.get_csv_data('./data/mock_matches.csv')
        matches_count = ipl_info.count_matches_per_year(matches)
        expected_output={'2015': 4, '2016': 2, '2017': 2}
        self.assertEqual(expected_output, matches_count)

    def test_matches_won(self):
        matches = utils.get_csv_data('./data/mock_matches.csv')
        matches_won = ipl_info.total_matches_won_by_team_each_year(matches)
        expected_output = {'Chennai Super Kings': {'2015': 2, '2016': 0, '2017': 0}, 'Delhi Daredevils': {'2016': 2, '2015': 0, '2017': 0}, 'Mumbai Indians': {'2017': 2, '2016': 0, '2015': 0}, 'Rajasthan Royals': {'2015': 2, '2016': 0, '2017': 0}}
        self.assertEqual(expected_output, matches_won)

    def test_extra_run_conceded_by_each_team(self):
        matches = utils.get_csv_data('./data/mock_matches.csv')
        deliveries = utils.get_csv_data('./data/mock_deliveries.csv')
        extra_run_conceded = ipl_info.get_extra_run_conceded_by_each_team('2015', matches, deliveries)
        expected_output={'Delhi Daredevils': 20, 'Chennai Super Kings': 16, 'Rajasthan Royals': 18}
        self.assertEqual(expected_output,extra_run_conceded)

    def test_economical_bowlers_in_year(self):
        matches = utils.get_csv_data('./data/mock_matches.csv')
        deliveries = utils.get_csv_data('./data/mock_deliveries.csv')
        economical_bowlers = ipl_info.get_economical_bowlers_in_year('2017', matches, deliveries)
        expected_output={'SL Malinga': 7.76, 'MJ McClenaghan': 10.73, 'JJ Bumrah': 7.56, 'KH Pandya': 6.75, 'Harbhajan Singh': 6.25, 'HH Pandya': 6.29}
        self.assertEqual(expected_output, economical_bowlers)

if __name__ == '__main__':
    unittest.main()