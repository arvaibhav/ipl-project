import csv
import matplotlib.pyplot as plt
import itertools
import random


def get_csv_data(path):
    with open(path, 'r') as file:
        csv_data = csv.DictReader(file)
        csv_list = []
        for row in csv_data:
            csv_list.append(dict(row))
        return csv_list


def plot_bar_chart(keys, values, title):
    plt.subplots(figsize=(25, 25))
    plt.bar(keys, values, width=0.2 if len(keys) >= 15 else .5)
    plt.suptitle(title)
    plt.xticks(fontsize=8, rotation=90 if len(keys) >= 10 else 0)
    plt.subplots_adjust(bottom=0.2, top=0.98)
    plt.show()


def plot_stacked_bar(teams_win_count_each_year, title):
    plt.subplots(figsize=(25, 25))
    plt.suptitle(title)
    colours = {}
    seasons = []

    for team, year_win_count in teams_win_count_each_year.items():
        colours[team] = "#{:06x}".format(random.randint(0, 0xFFFFFF))
        seasons = year_win_count.keys()

    seasons = sorted(seasons)
    bottom_coordinates = list(itertools.repeat(0, 10))

    for team in sorted(teams_win_count_each_year.keys()):
        matches_win = []
        flag = False
        for season in seasons:
            matches_win.append(teams_win_count_each_year[team][season])
        if flag is False:
            plt.bar(seasons, matches_win, color=colours[team], label=team, width=0.5, bottom=bottom_coordinates)
            bottom_coordinates = [a + b for a, b in zip(matches_win, bottom_coordinates)]
        else:
            plt.bar(seasons, matches_win, color=colours[team], label=team, width=0.5)
            flag = True

    plt.subplots_adjust(left=0.02)
    plt.legend(bbox_to_anchor=(.96, 1), loc=2, borderaxespad=0.)
    plt.grid()
    plt.show()
