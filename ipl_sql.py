import psycopg2 as pg2


class IplSql:
    conn = None

    def __init__(self):
        """To Initialize and establish the connection"""
        self.conn = pg2.connect(database="ipl_data", user="postgres", password="vaibhav", host="127.0.0.1", port="5432")

    def __del__(self):
        """To Close connection when object role complete"""
        self.conn.close()

    def get_data_from_query(self, query):
        """return query output from database"""
        cur = self.conn.cursor()
        cur.execute(query)
        data_from_query = cur.fetchall()
        return data_from_query

    def count_matches_per_year(self):
        query = """select distinct season ,count(season) 
                  from ipl.matches 
                  group by season;"""
        rows = self.get_data_from_query(query)
        return zip(*rows)

    def get_extra_run_conceded_by_each_team(self, year):
        query="""select a.bowling_team , sum(a.extra_runs) 
                from ipl.deliveries a 
                     inner join ipl.matches b 
                        on a.match_id =b.id 
                where b.season='{0}' 
                group by a.bowling_team;""".format(year)
        rows = self.get_data_from_query(query)
        return zip(*rows)

    def get_economical_bowlers_in_year(self, year):
        query="""select c.bowler , sum(c.sum*6/c.count) 
        from
        (select a.bowler ,sum(a.total_runs - a.bye_runs - a.legbye_runs ),count(a.ball)
        from ipl.deliveries a 
            inner join ipl.matches b 
                on a.match_id=b.id 
        where b.season='{0}' 
        group by a.bowler) 
        c group by c.bowler;""".format(year)
        rows = self.get_data_from_query(query)
        return zip(*rows)

    def get_macthes_won_percentage_by_each_team_in_toss_decision(self,toss_decision):
        query="""select c.toss_winner ,sum(ROUND((CAST(c.win_counts as float)/CAST(c.total as float))*100)) 
        from
        (select toss_winner, sum(case when toss_winner = winner then 1 else 0 end )as win_counts ,count(1) as total
        from ipl.matches 
        where toss_decision ='{0}'
        group by toss_winner
        )as c 
        group by c.toss_winner
        """.format(toss_decision)
        rows = self.get_data_from_query(query)
        return zip(*rows)

    def total_matches_won_by_team_each_year(self):
        query="""select winner , season ,count(winner) 
        from ipl.matches 
        group by winner,season
        order by winner,season ;
        """
        rows = self.get_data_from_query(query)
        matches_won = {}
        seasons_set = set()
        for row in rows:
            winner,season,winner_count=row[0],row[1],row[2]
            if winner is not None:
                seasons_set.add(season)
                if winner in matches_won:
                    matches_won[winner][season] = winner_count
                else:
                    temp = {season: winner_count}
                    matches_won[winner] = temp

        for season in seasons_set:
            for team in matches_won.values():
                if season not in team:
                    # adding and giving 0 won of season on which team dint played
                    team[season] = 0
        return matches_won

