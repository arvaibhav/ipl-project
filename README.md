# ipl-data-project

IPL Dataset Part 1
###     javascript program that will transform the raw csv data into a data structure in a format suitable for plotting with highcharts.

#   Generate the following plots ...

*   1. Plot the number of matches played per year of all the years in IPL.
*   2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
*   3. For the year 2016 plot the extra runs conceded per team.
*   4. For the year 2015 plot the top economical bowlers.