#
def count_matches_per_year(matches):
    count = {}
    for match in matches:
        if match['season'] is not '':
            if match['season'] in count:
                count[match['season']] += 1
            else:
                count[match['season']] = 1
    return count

#
def get_extra_run_conceded_by_each_team(year, matches, deliveries):
    extra_run_conceded_by_each_team = {}
    for match in matches:
        if match['season'] == year:
            for delivery in deliveries:
                extra_run=int(delivery['extra_runs'])
                if delivery['match_id'] == match['id']:
                    if delivery['bowling_team'] in extra_run_conceded_by_each_team:
                        extra_run_conceded_by_each_team[delivery['bowling_team']] += extra_run
                    else:
                        extra_run_conceded_by_each_team[delivery['bowling_team']] = extra_run
    return extra_run_conceded_by_each_team


#
def get_economical_bowlers_in_year(year, matches, deliveries):
    economical_bowlers = {}
    for match in matches:
        if match['season'] == year:
            for delivery in deliveries:
                if delivery['match_id'] == match['id']:
                    total_runs, bye_runs, legbye_runs = int(delivery['total_runs']), int(delivery['bye_runs']), int(delivery['legbye_runs'])
                    bowler = delivery['bowler']
                    if bowler in economical_bowlers:

                        economical_bowlers[bowler]['total_runs'] += total_runs - bye_runs - legbye_runs
                        economical_bowlers[bowler]['ball'] += 1
                    else:
                        economical_bowlers[bowler] = {'total_runs': total_runs - bye_runs - legbye_runs,
                                                                  'ball': 1}
    for eb in economical_bowlers.keys():
        total_runs, ball = economical_bowlers[eb]['total_runs'], economical_bowlers[eb]['ball']
        # after counting changing the value with respective economical bowlers rate
        economical_bowlers[eb] = round(total_runs * 6 / ball, 2)
    return economical_bowlers


#
def total_matches_won_by_team_each_year(matches):
    matches_won = {}
    seasons_set = set()
    for match in matches:
        if match['winner'] != '':
            seasons_set.add(match['season'])
            if match['winner'] in matches_won:
                match_season=match['season']
                if match_season in matches_won[match['winner']]:
                    matches_won[match['winner']][match_season] += 1
                else:
                    matches_won[match['winner']][match_season] = 1
            else:
                matches_won[match['winner']] = {match_season: 1}
    for season in seasons_set:
        for team in matches_won.values():
            # adding and giving 0 won of season on which team dint played
            if season not in team:
                team[season] = 0
    return matches_won
